import bodyParser = require("body-parser");
import cors = require("cors");
import { Application } from "express";
import express = require("express");
import passport = require("passport");
import cookieParser = require("cookie-parser");
import expressSession = require("express-session");
const { Strategy } = require("passport-google-oauth2");

const ensureAuthenticated = (req, res, next) => {
    if (req.isAuthenticated()) { return next(); }
    res.redirect('/login');
};

export const createServer = (apiKey, apiSecret) => {
    const app: Application = express();

    passport.serializeUser((user, done) => {
        console.log('Serialize', user);
        done(null, user);
    });

    passport.deserializeUser((obj, done) => {
        console.log('Deserialize', obj);
        done(null, obj);
    });

    passport.use(new Strategy({
            clientID:     apiKey,
            clientSecret: apiSecret,
            callbackURL: "http://localhost:3000/auth/google/callback",
            passReqToCallback   : true
        },
        (request, accessToken, refreshToken, profile, done) => {
            process.nextTick( () => {
                return done(null, profile);
            });
        }
    ));

    app.use(cors());
    app.use(cookieParser());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(expressSession({ secret: 'secret', resave: true, saveUninitialized: true }));

    app.use( passport.initialize());
    app.use( passport.session());

    app.get('/', (req, res) => {
        console.log(req.user);
        res.send(req.user);
    });

    app.get('/account', ensureAuthenticated, (req, res) => {
        console.log(req.user);
        res.send(req.user);
    });

    app.get('/login', (req, res) => {
        res.send(req.user);
    });

    app.get('/auth/google', passport.authenticate('google', { scope: [
        'https://www.googleapis.com/auth/plus.login',
        'https://www.googleapis.com/auth/plus.profile.emails.read']
    }));

    app.get( '/auth/google/callback',
        passport.authenticate( 'google', {
            successRedirect: '/',
            failureRedirect: '/login'
        }));

    app.get('/logout', (req, res) => {
        req.logout();
        res.redirect('/');
    });

    return app;
};