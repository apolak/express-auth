import { createServer } from "./src/server";

require("dotenv").config();

const app = createServer(process.env.API_KEY, process.env.API_SECRET);

app.listen(3000);